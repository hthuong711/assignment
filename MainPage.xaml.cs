﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using DauPhaiYoutube.Model;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DauPhaiYoutube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private YouTubeService youtubeService =
          new YouTubeService(new BaseClientService.Initializer
          {
              ApiKey = "AIzaSyDBf8bq5AKUSHfF_CF0eeZ2RCLzyfmOi5s",
              ApplicationName = "DauPhaiYoutube"
          });
        List<VideoEntity> ListVideo = new List<VideoEntity>();
        private string TokenNextPage = null, TokenPrivPage = null;

        public MainPage()
        {
            this.InitializeComponent();
            GetVideo();
        }

        private async void GetVideo(string PageToken = null)
        {
            var Request = youtubeService.Search.List("snippet");
            Request.ChannelId = "UC2X8usn1wtK85YcykPBQXwA";
            Request.MaxResults = 50;
            Request.Type = "video";
            Request.Order = SearchResource.ListRequest.OrderEnum.Date;
            Request.PageToken = PageToken;
            var Result = await Request.ExecuteAsync();
            if (Result.NextPageToken != null)
                TokenNextPage = Result.NextPageToken;
            if (Result.PrevPageToken != null)
                TokenPrivPage = Result.PrevPageToken;

            foreach (var item in Result.Items)
            {
                ListVideo.Add(new VideoEntity
                {
                    Title = item.Snippet.Title,
                    Id = item.Id.VideoId,
                    Img = item.Snippet.Thumbnails.Default__.Url
                });
            }
            lv.ItemsSource = null;
            lv.ItemsSource = ListVideo;
        }
        private async void mySearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = "AIzaSyDBf8bq5AKUSHfF_CF0eeZ2RCLzyfmOi5s",
                ApplicationName = "DauPhaiYoutube"
            });
            var listSearchVideos = youtubeService.Search.List("snippet");
            listSearchVideos.Q = mySearchBox.QueryText; //keywork search
            listSearchVideos.MaxResults = 25;

            var searchListResponse = await listSearchVideos.ExecuteAsync();
            List<VideoEntity> listVideos = new List<VideoEntity>();
            //List<string> listChannels = new List<string>();
            //List<string> listPlaylists = new List<string>();
            foreach (var searchResult in searchListResponse.Items)
            {
                switch (searchResult.Id.Kind)
                {
                    case "youtube#video":
                        listVideos.Add(new VideoEntity
                        {
                            Title = searchResult.Snippet.Title,
                            Id = searchResult.Id.VideoId,
                            Img = searchResult.Snippet.Thumbnails.Default__.Url
                        });
                        break;
                }
            }
            lv.ItemsSource = null;
            lv.ItemsSource = listVideos;
        }
        private void lv_ItemClick(object sender, ItemClickEventArgs e)
        {
            VideoEntity video = e.ClickedItem as VideoEntity;
            Frame.Navigate(typeof(VideoFrame), video);
        }
    }
}
